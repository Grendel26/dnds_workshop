
# Workshop evo-mol LBBE

LBBE evo-mol group

# Description

This repo tracks notes I’ve taken during the workshop session. You can
find them in markdown or rendered form in the `sessions` directory.

Suggest modifs by forking/pull request or using the `EDIT` button.

# Video links

- session 1: dN/dS usages in mol evol
  - Recording link: https://univ-lyon1.webex.com/univ-lyon1/ldr.php?RCID=be6be8a7a2df4afd8a2b85bc9b006857link:
  - Webex Password: `CgsGzi98`
- session 2: Coding Sequence Alignment
  - Recording link: https://univ-lyon1.webex.com/univ-lyon1/ldr.php?RCID=3b7e9712112a16a33d4a1d2b74d88c07
  - Password: `garfieldthe---c8T`
- session 3: Codon models
  - Recording link: https://univ-lyon1.webex.com/univ-lyon1/ldr.php?RCID=1a46f78464a687e51dd833606f5923c1
  - Password: `pDju3mkZ`
- session 4: Substitution mapping (to be added)
- session 5: Detecting selection with codon models
  - Recording link: https://univ-lyon1.webex.com/univ-lyon1/ldr.php?RCID=a5687066984749a2a1a516dbd9169c97
  - Password: `eDema2pK`
- session 6: Comparative analysis of dS and dN/dS
  - Recording link: https://univ-lyon1.webex.com/univ-lyon1/ldr.php?RCID=89ef592ba870c2874b157eb7e978abca
  - Password: nRmA2YnQ

Vids were recorded in French.
