---
title: "Coding Sequences Alignment"
author: "g. perrière & s. barreto"
date: "2021-02-24 16:31"
output: tint::tintPdf
bibliography: references.bib
latexfonts:
  - package: AlegreyaSans
    options: sfdefault
  - package: FiraMono
    options: scaled=0.7
---

## Introduction

This session was dedicated to an overview of the program tailored for
coding sequence alignment and post-processing. Guy works mostly on sequence alignment, but not that much on *coding* sequence *per se*. This session was oriented towards pros and cons of alignment software, and why some are better than others in the specific context of dN/dS analysis.

Guy put up a toy dataset with the Chalcone Synthase dataset from @YangLikelihoodAnalysisChalcone2004, one of which was tweaked a bit to introduce INDELs (files `data/input/CHS{,-del}.fst`).

> The following is taken from my (SB) notes; Guy’s presentation is
> available
> [here](https://gitlab.com/hrz-project/dnds_workshop/-/blob/master/sessions/session2/guy_alignement_cds.pdf).
> Additional materials are available in the `session2` directory.

## How do we align coding sequences?

A correct CDS alignment must follow the codon-structure of the sequence.
Non-homologous positions will be aligned if we used “usual” alignment softwares.
One way to go round this is to translate the coding sequence in amino-acid sequences, align them under AA-form and back-translate.

The main problem with this approach has to do with partial sequences;
we must sometimes have to resort to manual editing to put the coding
sequence in phase. `SeaView` or `pal2nal.pl` can automate this
procedure a bit. Another problem: sequence can contain in-phase stop
codons, usually due to draft alignment sequences, or partial
sequences.

> We need coding-sequence oriented programs to deal with these issues.

The MACSE program aims to solve these. The usual iterative procedure during a multiple sequence alignment is Needleman-Wunsch based; MACSE translate the sequence in three phases, introducing *in* the alignment procedure the idea that there might be some frameshift, be it artefactual (sequencing errors) or biological (frameshift mutations are well known in a classical example in *E. coli*, for instance, as Guy mentionned.)

PRANK and MACSE are the only tools able to maintain the codon-structure *during* the alignment procedure.

## Progressive alignment

This is a procedure in use since @FengProgressiveSequenceAlignment1987: a guide-tree is built from initial  pairwise-derived distance measures, with successive profile to profile alignment.
All kinds of algorithms have been designed to speed-up the initial distance matrice construction.

Consistance-constraints have been introduced to increase the quality of an alignment.
They are mostly based on the 3D conformation of infered proteins (T-Coffee).

The guide-tree is built by hierarchical clustering (UPGMA for instance) of pairwise distances measured by Needleman-Wunsch algorithm (global alignment).
The NW algorithm returns the best possible alignment between two sequences for a given scoring function.
Errors introduced at the pairwise alignment step are back-propagated to the rest of the tree, in case no betterment is applied downwards.
Consistence constraints deals with reducing this kind of primitive errors.
Notredame & Higgins have introduced in T-Coffee consistance procedures that counts the number of NW-alignment triplets maximising a consistance score. The procedure has a $O(n^2)$ complexity however.

MAFFT also has procedures for maintaining consistency (the LGE-INS alignment modes).
Pairwise alignment does not necessarily needs a NW procedure. Smith-Waterman procedures gives local alignment, and are used by the {L,E}-ins procedures in MAFFT.
MAFFT typically is 50 times faster than T-Coffee, with essentially comparable results.

Most alignment benchmarking are based on structural characters, and are not evolutionnary (phylogenetically) based.
They typically compare the known structure of a protein to the structure predicted by the alignment.
PRANK is generally not well ranked in these kinds of benchmarks. That could be due to its intrinscally evolutionnary-oriented procedures.^[I should mention this paper by @PervezSAliBASEDatabaseSimulated2019, that built simulated alignment to see how well aligners could reconstruct the true homologies between sites, rather than focusing on their structural properties.]

The nature of these benchmarks raise the question of their reliability in an evolutionnary context; we could easily imagine evolutionnary process that would *generate* 3D-structures instability.
Nicolas suggested that Prank chose not to align some unalignable sites, but it might be that aligned positions are more reliably aligned.

MACSE attempts to deal with the fact that there might be artefactuals INDELs.
It has *never* been tested in comparative alignment programs benchmark.
Guy finds MACSE a bit slow.

## Alignment filtering

> Garbage In, Garbage Out.

Filtering is the process of chosing sites or rows in an alignment that
are probably effectively homologous.
Two strategies competes: the “take it or leave it” (TILI) strategy masks the entire column or the entire row if it is rejected; the “Picky” strategy masks regions only (short rectangles).

Only BMGE, OMM\_MACSE and TrimAl are able to deal directly with coding sequences; they translate it to AA internally. BMGE sort codons, not nucleotides.

A much debated paper by the C. Dessimoz team[@TanCurrentMethodsAutomated2015] showed that filtering sequence alignement tended to worsen gene tree inferences.
They mostly dealt with tree topology (branching patterns), not with branch length (evolutionnary rates).

@RanwezStrengthsLimitsMultiple2020 have compared the phylogenies
derived from filtered or unfiltered alignment in CDS from 116 mammals,
by measuring the topological distance (quartet methods) to a reference
species tree obtained from the whole OrthoMam dataset. They showed
that only Picky-based (HmmCleaner or OMM\_MACSE) approaches
consistently lead to the “correct” tree, with noticeable effects on
branch lengths.

# Practice

## Software install

Softwares needed:

|                                                    |                                                                                                |
|---------------------------------------------------:|:-----------------------------------------------------------------------------------------------|
|   [SeaView](http://doua.prabi.fr/software/seaview) | [Muscle](inclu dans la distribution de SeaView)                                                |
| [MAFFT](https://mafft.cbrc.jp/alignment/software/) | [PRANK](http://wasabiapp.org/software/prank/)                                                  |
|     [MACSE](https://bioweb.supagro.inra.fr/macse/) | [Pal2Nal](http://www.bork.embl.de/pal2nal/)                                                    |
|        [MstatX](https://github.com/gcollet/MstatX) | [BMGE](https://research.pasteur.fr/fr/software/bmge-block-mapping-and-gathering-with-entropy/) |


Fetch MACSE java archive and alias it for convenience, assuming you are using bash/zsh shells.

``` shell
wget https://bioweb.supagro.inra.fr/macse/releases/macse_v2.05.jar
macse () { java -jar $(realpath macse_v2.05.jar) "$@" ; }
```

Fetch BMGE java archive and alias it for convenience too:

``` shell
wget http://ftp.pasteur.fr/pub/gensoft/projects/BMGE/BMGE-1.12.tar.gz
tar xvf BMGE-1.12.tar.gz
cd BMGE-1.12
bmge () { java -jar $(realpath BMGE.jar) "$@" ; }
```

Create a wrapper around the command line interface of Seaview (on macOS):

``` shell
seaview () { /Applications/Seaview.app/Contents/MacOS/seaview "$@" ; }
```

## Alignment

``` shell
git clone https://gitlab.com/hrz-project/dnds_workshop
cd dnds_workshop/data/input
```

Use Seaview to go from DNA to AA using the command line:

``` shell
seaview -convert -translate -o CHS-aa.fst CHS.fst
```

Align under protein with muscle form then backtranslate:

``` shell
muscle -in CHS-aa.fst -out CHS-muscle-aa.fst
pal2nal.pl CHS-muscle-aa.fst CHS.fst -output fasta > CHS-muscle.fst
```

Align with MAFFT then backtranslate:

``` shell
mafft --globalpair --maxiterate 1000 --thread 4 CHS-aa.fst > CHS-mafft-aa.fst
pal2nal.pl CHS-mafft-aa.fst CHS.fst -output fasta > CHS-mafft.fst
```

This MAFFT command line use the G-INS-i program (global pair alignment then iterative refinement with consistency constraints).

Direct codon alignment with PRANK:

``` shell
prank -codon -d=CHS.fst -o=CHS-prank
mv CHS-prank.best.fas CHS-prank.fst
```

PRANK doesn’t know how to deal with CDS if their length is not a multiple of 3: it doesn’t know how to put sequences in frame, in case of INDELs. See for instance:

``` shell
prank -codon -d=CHS-del.fst -o=CHS-del
```

Direct codon alignment with MACSE:

``` shell
macse -prog alignSequences -seq CHS.fst -out_NT CHS-macse.fst -out_AA CHS-macse-aa.fst
```

Demonstration that MACSE knows how to deal with INDELs:

``` shell
macse -prog alignSequences -seq CHS-del.fst -out_NT CHS-macse-del.fst -out_AA CHS-macse-del-aa.fst
```

## Quality measurement

Mstatx is a software for measure global alignment quality using the Trident method (column-conservation index):

```shell
export SCORE_MAT_PATH="$HOME/src/MstatX/data/aaindex"
mstatx -i CHS-muscle.fst -m ${SCORE_MAT_PATH}/DNA.mat -s trident -g -o CHS-muscle.stat
mstatx -i CHS-mafft.fst -m ${SCORE_MAT_PATH}/DNA.mat -s trident -g -o CHS-mafft.stat
mstatx -i CHS-prank.fst -m ${SCORE_MAT_PATH}/DNA.mat -s trident -g -o CHS-prank.stat
mstatx -i CHS-macse.fst -m ${SCORE_MAT_PATH}/DNA.mat -s trident -g -o CHS-macse.stat
```

Best alignment score is the one with MUSCLE, followed by MAFFT, then MACSE.
PRANK has the lowest score, but aligned more sites.

## Filtering / trimming

Preliminary trimming of the muscle alignment with default parameters of BMGE:

``` shell
bmge -i CHS-muscle.fst -t CODON -of CHS-muscle-bmge.fst
```

BMGE keeps 358 sites (check with SeaView) of the 1305 sites (27%). How
does a GBlocks filtering fare? Translate to protein, filter with
GBlocks and backtranslate in SeaView, with default GBlocks filtering
parameters (leave everything unchecked). GBlocks keeps 975 sites
(74%). BMGE is really stringent here.

To reduce BMGE’s stringency, one can:

- tweak the default substituton matrix (`-m` option). BMGE translate
  CDS to protein and then measure columns entropy. The BLOSUM62 matrix
  is used by default. So if one wants to reduce stringency, she should
  use a substitution matrix designed for lower similarity than
  BLOSUM62 (BLOSUM30 or 40 for instance).
- tweak the threshold for gaps frequency across a column (`-g`
  option). The highest this threshold the less stringent BMGE is.
- minimal block size (`-b` option). The lowest this value the less
  stringent.

Use this command for a less stringent filtering:

``` shell
bmge -i CHS-muscle.fst -t CODON -m BLOSUM30 -g 0.50 -b 2 -of CHS-muscle-lax.fst
```

This will return 31 more sites than the previous BMGE command with default parameters.

## Tree inference

To check how filtering could affect tree inference, we use iqtree
(version 1 here) to infer the tree for all alignment filtering used,
on the MSA derived by MUSCLE. Note that the sequences
`CHS-muscle-lax.fst` and `CHS-muscle-bmge.fst` must be edited with
SeaView so that the number of sites is a multiple of 3, otherwise
IQ-TREE would be at a loss trying to use a codon model.

Once these two sequences have been edited, tree inference is performed by:

``` shell
iqtree -s CHS-muscle.fst -st CODON -m GY+G4 -nt 4
iqtree -s CHS-muscle-gblocks.fst -st CODON -m GY+G4 -nt 4
iqtree -s CHS-muscle-lax.fst -st CODON -m GY+G4 -nt 4
iqtree -s CHS-muscle-bmge.fst -st CODON -m GY+G4 -nt 4
```

Here we used a Goldman & Yang model with a discrete Gamma law with four categories.
Once the four trees are infered, we can compare the topologies and see how they fare against the raw unfiltered alignement-derived tree.

All we need to do is concatenate the trees to the same file:

``` shell
cat CHS-muscle.fst.treefile CHS-muscle-gblocks.fst.treefile \
    CHS-muscle-lax.fst.treefile CHS-muscle-bmge.fst.treefile \
    > CHS-muscle.trees
```

and then use IQTREE to perform classical topology comparison tests (KH, SH, ELW and AU) with a fixed number of replicates to 10k (advised minimal value for the AU test).

``` shell
iqtree -s CHS-muscle.fst -st CODON -m GY+G4 -nt 4 -z CHS-muscle.trees -n 0 -zb 10000 -au -redo
```

This shows that the infered topologies *disagree* with the unfiltered
alignment derived topologies. This goes against the conclusion of
@TanCurrentMethodsAutomated2015 discussed above, at least for this
dataset.

# References
